// 3DScan.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{


    PXCSenseManager* senseManager = PXCSenseManager::CreateInstance();

    if (senseManager)
    {

        if (senseManager->Init() == PXC_STATUS_NO_ERROR)
        {

            UtilRender *render = new UtilRender(L"Streaming");
            bool quit = false;
            while (!quit)
            {

                pxcStatus status = senseManager->AcquireFrame(true);
                if (status != PXC_STATUS_NO_ERROR)
                    break;

               
                senseManager->ReleaseFrame();

                if (GetAsyncKeyState(VK_ESCAPE))
                    break;
            }

            delete render;
            senseManager->Close();
        }
        senseManager->Release();
    }

	return 0;
}

