// Projection.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"




void displayHandData(PXCHandData* handData)
{
	int handNumber = handData->QueryNumberOfHands();

	for (int i=0; i<handNumber; i++)
	{
		PXCHandData::IHand* ihand;
		handData->QueryHandData(PXCHandData::AccessOrderType::ACCESS_ORDER_BY_TIME, i, ihand);

		PXCHandData::JointData jointData;
		ihand->QueryTrackedJoint(PXCHandData::JointType::JOINT_CENTER, jointData);

		pxcF32 x = jointData.positionImage.x;
		pxcF32 y = jointData.positionImage.y;

		system("cls");
		wprintf(L"hand %d plam center at x=%f y=%f\n", i, x, y);
	}
}

int _tmain(int argc, _TCHAR* argv[])
{

	PXCSenseManager* senseManager = PXCSenseManager::CreateInstance();
	if (senseManager)
	{
		senseManager->EnableHand();

		if (senseManager->Init() == PXC_STATUS_NO_ERROR)
		{	

			PXCHandModule* handModule = senseManager->QueryHand();
			PXCHandData* handData = handModule->CreateOutput();
			PXCHandConfiguration* handConfigure = handModule->CreateActiveConfiguration();
			if (handData && handConfigure)
			{	
				

				bool quit = false;
				UtilRender *render = new UtilRender(L"Streaming");
				while (!quit)
				{
					if (senseManager->AcquireFrame(true) != PXC_STATUS_NO_ERROR)
						break;
					handData->Update();

					displayHandData(handData);
					if (!render->RenderFrame(senseManager->QuerySample()->depth))
						quit = true;

					senseManager->ReleaseFrame();
				}
				delete render;

				handData->Release();
				handConfigure->Release();
			}

			senseManager->Close();
		}

		senseManager->Release();
	}

	system("pause");
	return 0;
}

