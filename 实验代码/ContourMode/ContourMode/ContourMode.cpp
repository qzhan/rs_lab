// ContourMode.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{

	PXCSenseManager* senseManager = PXCSenseManager::CreateInstance();
	if (senseManager)
	{

		

		if (senseManager->Init() == PXC_STATUS_NO_ERROR)
		{

			bool quit = false;
			UtilRender *render = new UtilRender(L"Streaming");
			while (!quit)
			{
				if (senseManager->AcquireFrame(true) != PXC_STATUS_NO_ERROR)
					break;

				PXCCapture::Sample* sample = senseManager->QuerySample();

				if (!render->RenderFrame(sample->depth))
					quit = true;

				senseManager->ReleaseFrame();
			}
			delete render;

			senseManager->Close();
		}

		senseManager->Release();
	}

	system("pause");

	return 0;
}

